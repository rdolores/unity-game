﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reloj : MonoBehaviour
{
    [Tooltip("Tiempo inicial en segundos")]
    public int initialTime;

    [Tooltip("Escala del tiempo del Reloj")]
    [Range(-10.0f,10.0f)]
    public float timeScale = 1;

    private Text myText;
    private float timeFrameScale = 0f;
    private float timeInSecondsToShow = 120f;
    private float timeScaleWhenPaused, initialTimeScale;
    private bool isPaused;

    // Start is called before the first frame update
    void Start()
    {
        //Establecer escala de tiempo original
        initialTimeScale = timeScale;

        //Obtener el componente Texto
        myText = GetComponent<Text>();

        //Inicializamos la variable que acumula los tiempos de cada frame con el tiempo inicial
        timeInSecondsToShow = initialTime;

        UpdateClock(initialTime);
    }

    // Update is called once per frame
    void Update()
    {
        //La siguiente variable representa el tiempo de cada frame considerando la escala de tiempo
        timeFrameScale = Time.deltaTime * timeScale;
        //La siguiente variable va acumulando el tiempo transcurrido para luego mostrarlo en el reloj
        timeInSecondsToShow += timeFrameScale;
        UpdateClock(timeInSecondsToShow);
    }

    public void UpdateClock(float timeInSeconds) {
        int minutes = 0;
        int seconds = 0;
        string clockText;

        //Validar que tiempo no sea negativo
        if(timeInSeconds < 0) timeInSeconds = 0;

        //Calcular minutos y segundos
        minutes = (int)timeInSeconds / 60;
        seconds = (int)timeInSeconds % 60;

        //Crear la cadena de caracteres con 2 digitos para los minutos y segundos, separados por ":"
        clockText = minutes.ToString("00") + ":" + seconds.ToString("00");

        //Actualizar elemento de texto de UI con la cadena de caracteres
        myText.text = clockText;
    }

    public float getTimeInSecondsToShow() { return timeInSecondsToShow; }
    public Text getMyText() { return myText; }
}
