﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject menuPrincipal;
    public GameObject timer;
    public GameObject menuGameOver;
    public Reloj reloj;
    public GameObject piedra1;
    public GameObject piedra2;
    public float velocidad = 2;
    public GameObject col;
    public Renderer fondo;
    public bool timerExpired;
    public bool gameOver;
    public bool start;
    public List<GameObject> cols;
    public List<GameObject> obstacles;
    // Start is called before the first frame update
    void Start()
    {
        //Creacion de mapa
        for(int i = 0; i < 21; i++) {
            cols.Add(Instantiate(col, new Vector2(-10 + i, -3), Quaternion.identity));
        }

        //Creacion de piedras
        obstacles.Add(Instantiate(piedra1, new Vector2(14, -2), Quaternion.identity));
        obstacles.Add(Instantiate(piedra2, new Vector2(18, -2), Quaternion.identity));
    }

    // Update is called once per frame
    void Update()
    {
        if(!start) {
            if(Input.GetKeyDown(KeyCode.X)) {
                start = true;
            }
        }

        if(start && reloj.getTimeInSecondsToShow() <= 1) {
            timerExpired = true;
        }

        if(start && timerExpired) {
            timer.SetActive(false);
            menuPrincipal.SetActive(true);
            if(Input.GetKeyDown(KeyCode.X)) {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if(start && gameOver) {
            timer.SetActive(false);
            menuGameOver.SetActive(true);
            
            if(Input.GetKeyDown(KeyCode.X)) {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        if(start && !gameOver && !timerExpired) {
            menuPrincipal.SetActive(false);
            timer.SetActive(true);
            fondo.material.mainTextureOffset = fondo.material.mainTextureOffset + new Vector2(0.02f, 0) * Time.deltaTime;
        
            //Movimiento de mapa
            for(int i = 0; i < cols.Count; i++) {

                if(cols[i].transform.position.x <= -10) {
                    cols[i].transform.position = new Vector3(10, -3, 0);
                }

                cols[i].transform.position =  cols[i].transform.position + new Vector3(-1, 0, 0) * Time.deltaTime * velocidad;
            }

            //Movimiento de piedras
            for(int i = 0; i < obstacles.Count; i++) {

                if(obstacles[i].transform.position.x <= -10) {
                    float randomObs = Random.Range(11, 18);
                    obstacles[i].transform.position = new Vector3(randomObs, -2, 0);
                }

                obstacles[i].transform.position =  obstacles[i].transform.position + new Vector3(-1, 0, 0) * Time.deltaTime * velocidad;
            } 
        }
    }
}
